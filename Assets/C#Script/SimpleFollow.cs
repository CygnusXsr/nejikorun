﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleFollow : MonoBehaviour {
    //diffは違い
    Vector3 diff;
    //追従ターゲットパラメータ
    public GameObject target;
    public float followSpeed;
    void Start () {
        //追従距離の計算
        diff = target.transform.position - transform.position;
    }
    //キャラクターが動いた後にカメラも動かすためにLateUpdate()を使っている
    void LateUpdate () {
        //線形補間関数によるスムージング
        transform.position = Vector3.Lerp(
        transform.position,target.transform.position - diff,Time.deltaTime * followSpeed);
    }
}
